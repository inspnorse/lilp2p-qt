# Simple p2p broadcast using PubSub and GossipSub

This program demonstrates a simple p2p broadcast application using GossipSub implementation of PubSub protocol.

## Build

From the `go-libp2p-examples/pubsub` directory run the following:

```
> go build
```

## Usage

Use a terminal to run this command:

```
./pubsub
```

## Author
1. [Mahdi Bakhshi](https://github.com/MBakhshi96)